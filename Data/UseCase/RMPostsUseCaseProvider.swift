//
//  RMPostsUseCaseProvider.swift
//  RealmCleanArchitectureRx
//
//  Created by drjoy on 2/7/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class RMPostsUseCaseProvider: PostsUseCaseProvider {
    
    private let configuration: Realm.Configuration

    public init(configuration: Realm.Configuration = Realm.Configuration()) {
        self.configuration = configuration
    }
    
    func makePostsUseCase() -> PostsUseCase {
        let respository = Respository<Post>(configuration: configuration)
        return RMPostsUseCase(repository: respository)
    }
}
