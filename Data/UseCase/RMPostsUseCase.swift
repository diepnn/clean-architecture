//
//  RLPostsUseCase.swift
//  RealmCleanArchitectureRx
//
//  Created by drjoy on 2/7/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import Foundation
import RxSwift
import Realm
import RealmSwift
import RxRealm

class RMPostsUseCase<Respository>: PostsUseCase where Respository: AbstractRepository, Respository.T == Post {
    
    private let repository: Respository

    init(repository: Respository) {
        self.repository = repository
    }
    
    func posts() -> Observable<[Post]> {
        return repository.queryAll()
    }
    
    func delete(_ post: Post) -> Observable<Void> {
        return repository.delete(entity: post)
    }
    
    func addPost(_ newPost: Post) -> Observable<Void> {
        return repository.save(entity: newPost)
    }
}
