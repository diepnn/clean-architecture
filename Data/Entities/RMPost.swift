//
//  RMPost.swift
//  RealmCleanArchitectureRx
//
//  Created by drjoy on 2/6/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import Realm
import RealmSwift
import QueryKit

class RMPost: Object {
    @objc dynamic var uid: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var body: String = ""
    
    override class func primaryKey() -> String? {
        return "uid"
    }
}

extension RMPost {
    static var title: Attribute<String> { return Attribute("title")}
    static var body: Attribute<String> { return Attribute("body")}
    static var uid: Attribute<String> { return Attribute("uid")}
}

extension RMPost: DomainConvertibleType {
    
    func asDomain() -> Post {
        return Post(title: title,
                    body: body,
                    uid: uid)
    }
}

extension Post: RealmRepresentable {
    
    func asRealm() -> RMPost {
        return RMPost.build{ object in
            object.title = title
            object.uid = uid
            object.body = body
        }
    }
}
