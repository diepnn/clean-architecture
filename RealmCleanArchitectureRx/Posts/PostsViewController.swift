//
//  ViewController.swift
//  RealmCleanArchitectureRx
//
//  Created by drjoy on 2/5/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class PostsViewController: UIViewController {
    typealias ViewModel = PostsViewModel

    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var actionAddNewPost: UIButton!
    
    var viewModel: ViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        bindViewModel()
    }
    
    private func configTableView() {
        tableView.refreshControl = UIRefreshControl()
        tableView.estimatedRowHeight = 88
        tableView.rowHeight = UITableView.automaticDimension
    }

    private var dataSources = [PostsSectionData]()

    private func bindViewModel() {
        assert(viewModel != nil)
        
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let pull = tableView.refreshControl!.rx
            .controlEvent(.valueChanged)
            .asDriver()

        tableView.rx.setDelegate(self).disposed(by: disposeBag)
        tableView.rx.setDataSource(self).disposed(by: disposeBag)
        
        let input = PostsViewModel.Input(trigger: Driver.merge(viewWillAppear, pull),
                                         createPostTrigger: actionAddNewPost.rx.tap.asDriver(),
                                         selection: tableView.rx.itemSelected.asDriver())
        let output = viewModel.transform(input: input)

        output.posts.drive(onNext: { [weak self](posts) in
            guard let `self` = self else { return }
            self.dataSources = posts.map({ (post) -> PostsSectionData in
                return PostsSectionData(header: post.title, items: [post], isExpanded: true)
            })
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
        output.selectedPost.drive()
            .disposed(by: disposeBag)
        output.createPost.drive()
            .disposed(by: disposeBag)
        output.fetching.drive(tableView.refreshControl!.rx.isRefreshing)
            .disposed(by: disposeBag)
        
    }

    func expanded(at section: Int) {

    }
}

extension PostsViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataSources.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionModel = self.dataSources[section]
        return sectionModel.isExpanded ? sectionModel.items.count : 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(ofType: PostTableViewCell.self, at: indexPath)
        cell.bind(dataSources[indexPath.section].items[indexPath.row])
        return cell
    }
}

extension PostsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = PostsSectionHeaderView(shouldSetup: true)
        view.labelTitle.text = dataSources[section].header
        view.arrowDown.image = UIImage(named: dataSources[section].isExpanded ? "arrrow_up" : "arrrow_down")
        view.buttonExpaned.rx.tap.asDriverOnErrorJustComplete().drive(onNext: { [weak self](_) in
            guard let `self` = self else { return }
            self.dataSources[section].isExpanded = !self.dataSources[section].isExpanded
            self.tableView.reloadData()

        }).disposed(by: disposeBag)
        return view
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
