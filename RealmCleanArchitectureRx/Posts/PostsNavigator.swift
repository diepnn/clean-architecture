//
//  PostNavigator.swift
//  RealmCleanArchitectureRx
//
//  Created by drjoy on 2/5/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import Foundation
import UIKit

protocol PostNavigatorProtocol {
    func toPosts()
    func toCreatePost()
    func toEditPost(_ toPost: Any)
}

class PostsNavigator: PostNavigatorProtocol {
    
    private let storyBoard: UIStoryboard!
    private let services: PostsUseCaseProvider
    private let navigationController: UINavigationController

    init(services: PostsUseCaseProvider,
         navigationController: UINavigationController,
         storyBoard: UIStoryboard) {
        self.services = services
        self.navigationController = navigationController
        self.storyBoard = storyBoard
    }
    
    func toPosts() {
        let vc = storyBoard.instantiateViewController(identifier: "posts") as? PostsViewController
        vc?.viewModel = PostsViewModel(useCase: services.makePostsUseCase(), navigator: self)
        navigationController.pushViewController(vc!, animated: true)
    }
    
    func toCreatePost() {
        
    }
    
    func toEditPost(_ toPost: Any) {
        
    }
}
