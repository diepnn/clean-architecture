//
//  PostsViewModel.swift
//  RealmCleanArchitectureRx
//
//  Created by drjoy on 2/7/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class PostsViewModel: ViewModelType {
    
    private let useCase: PostsUseCase
    private let navigator: PostsNavigator
    
    init(useCase: PostsUseCase, navigator: PostsNavigator) {
        self.useCase = useCase
        self.navigator = navigator
    }
    
    struct Input {
        let trigger: Driver<Void>
        let createPostTrigger: Driver<Void>
        let selection: Driver<IndexPath>
    }
    
    struct Output {
        let fetching: Driver<Bool>
        let posts: Driver<[Post]>
        let createPost: Driver<Void>
        let selectedPost: Driver<Post>
        let error: Driver<Error>
    }
    
    func transform(input: Input) -> Output {
        
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        let posts = useCase.posts()
            .asDriverOnErrorJustComplete()
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        let selectedPost = input.selection.withLatestFrom(posts) { (indexPath, posts) in
            return posts[indexPath.row]
        }.do(onNext: { (post) in
            self.navigator.toEditPost(post)
        })
        
        let addNewPost = input.createPostTrigger.do(onNext: { (_) in
            self.navigator.toCreatePost()
        })
        
        return Output(fetching: fetching,
                      posts: posts,
                      createPost: addNewPost,
                      selectedPost: selectedPost,
                      error: errors)
    }


}
