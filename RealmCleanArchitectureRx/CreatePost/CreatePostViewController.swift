//
//  CreatePostViewController.swift
//  RealmCleanArchitectureRx
//
//  Created by Diep Nguyen on 2/10/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CreatePostViewController: UIViewController, ViewModelType {
    typealias ViewModel = CreatePostViewModel

    var viewModel: ViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }

    private func bindViewModel() {
        assert(viewModel != nil)
    }
}
