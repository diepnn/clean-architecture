//
//  CreatePostNavigator.swift
//  RealmCleanArchitectureRx
//
//  Created by drjoy on 2/7/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import Foundation
import UIKit

protocol CreatePostNavigatorProtocols {

    func toPosts()
}

class CreatePostNavigator: CreatePostNavigatorProtocols {

    private let navigationController: UINavigationController
    init(navigation: UINavigationController) {
        navigationController = navigation
    }

    func toPosts() {
        navigationController.dismiss(animated: true, completion: nil)
    }
}
