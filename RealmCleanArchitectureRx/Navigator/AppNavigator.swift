//
//  AppNavigator.swift
//  RealmCleanArchitectureRx
//
//  Created by drjoy on 2/5/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import UIKit

class AppNavigator {
    static let shared = AppNavigator()
    let postUseCaseProvider: PostsUseCaseProvider
    
    init() {
        postUseCaseProvider = RMPostsUseCaseProvider()
    }
    
    func configureRoot(in windown: UIWindow) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let postsNavigation = UINavigationController()
        let postNavigator = PostsNavigator(services: postUseCaseProvider,
                                          navigationController: postsNavigation,
                                          storyBoard: storyboard)
        windown.rootViewController = postsNavigation
        postNavigator.toPosts()
    }
}
