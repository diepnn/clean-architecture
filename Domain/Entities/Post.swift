//
//  Posts.swift
//  RealmCleanArchitectureRx
//
//  Created by drjoy on 2/6/20.
//  Copyright © 2020 drjoy. All rights reserved.
//

import Foundation

struct Post: Codable {
    let title: String
    let body: String
    let uid: String
    
    init(title: String, body: String, uid: String = UUID().uuidString) {
        self.title = title
        self.body = body
        self.uid = uid
    }
}
